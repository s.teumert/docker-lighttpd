# Changelog

# 1.0.3

* Fix serving of index file

# 1.0.2

* Fix incorrect ssl build

## 1.0.1

* Move `/docker-entrypoint.sh` to `/user/local/bin/lighttpd-docker`
  * Don't pollute `/`
  * Avoid naming conflicts
* Add `maintainer` metadata

## 1.0.0

* Initial release, including no-ssl (default) and ssl variants
