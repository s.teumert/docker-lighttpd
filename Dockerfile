FROM alpine:3.12

LABEL maintainer="Sebastian Teumert <sebastian.teumert@gmx.de>"

# Install packages
RUN apk update && \
    apk add --no-cache \
    lighttpd \
    bash \
    curl && \
    rm -rf /var/cache/apk/*

# copy config
COPY lighttpd.conf /etc/lighttpd/

# create cache/compress dir
RUN mkdir -p /var/lib/lighttpd/cache/compress && chown lighttpd:lighttpd /var/lib/lighttpd/cache/compress

# copy entry point script
COPY lighttpd-docker /usr/local/bin/

HEALTHCHECK --interval=15s --timeout=1s \
  CMD curl -f https://localhost/ || exit 1

# Expose http port
EXPOSE 80

VOLUME /var/www/localhost/htdocs
VOLUME /etc/lighttpd

ENTRYPOINT ["lighttpd-docker"]