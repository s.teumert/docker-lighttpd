# docker-lighttpd

Easy-to-use, production-ready lighttpd container.

[![polygnome/lighttpd:latest](https://badgen.net/badge/docker/polygnome%2Flighttpd:latest?icon=docker)](https://hub.docker.com/r/polygnome/lighttpd)

## Features

* Very lightweight at less then 12 MB (< 6MB compressed)
* Production ready for static files, just pass a volume as `htdocs` directory and
  it is ready to go
* *Error log* is redirected to docker and available in docker logs

### SSL / TLS 1.3

In the `ssl` directory, a variant of lighttpd with `mod_openssl` is available.
The variant offers:

* A self-signed SSL certificate (created at build time)
* Forced redirect from HTTP to HTTPS

It is recommended to build that variant locally or to provide a custom SSL
certificate. For that, you specify `-v <your-certs-dir>:/etc/lighttpd/certs`.
Your directory has to contain a `localhost.pem` file.

## Usage

* Mount your document root to `/var/www/localhost/htdocs`
* If you want to use custom configs, mount the folder to `/etc/lighttpd`
* If you use ssl and want to bring your own certificate, mount the folder to `/etc/lighttpd/certs`

If you want the access log piped through to the docker log, include a `lighttpd.conf` with

    accesslog.filename = "/tmp/lighttpd_log"

## Contents

* lighttpd `1.4.55-r1`
* `bash`
* `curl`

# License

(The MIT License)

Copyright &copy; 2020 Sebastian Teumert &lt;https://sebastian.teumert.net&gt;

[See LICENSE](/LICENSE)
